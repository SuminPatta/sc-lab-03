import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;


public class TokenizerGUI {
	
	public String firstDialog(){
		String str1 = "";
		JTextArea Field = new JTextArea(5,15);
		JPanel myPanel = new JPanel();
	    myPanel.add(new JLabel("Enter a String :"));
	    myPanel.add(Field);
	    int result = JOptionPane.showConfirmDialog(null, myPanel, "Enter", JOptionPane.OK_CANCEL_OPTION);
	    if (result == JOptionPane.OK_OPTION) {
			str1 = Field.getText();
	      }
		return str1;
	}
	public String secondDialog(){
		String str1 = "";
		JTextArea xField = new JTextArea(5,15);
		JPanel myPanel = new JPanel();
	     myPanel.add(new JLabel("Enter a String :"));
	      myPanel.add(xField);
		int result = JOptionPane.showConfirmDialog(null, myPanel, "Enter", JOptionPane.OK_CANCEL_OPTION);
		if (result == JOptionPane.OK_OPTION) {
			str1 = xField.getText();
	      }
		return str1;
	}
	
	public void showResult(String str){
		JOptionPane.showMessageDialog(null, str, "Enter", JOptionPane.OK_CANCEL_OPTION);
	}

}
