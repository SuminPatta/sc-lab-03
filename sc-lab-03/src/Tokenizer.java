import java.util.ArrayList;
import java.util.StringTokenizer;


public class Tokenizer {
	
	public ArrayList<String> stringTokenizer(String words)
	{
		ArrayList<String> list = new ArrayList<String>();
		String s1 = "";
		
		
		if(words.contains("\n"))
		{
			words = words.replace("\n", " ");
		}
		
		
		if(words.contains("			"))
		{
			words = words.replace("			", " ");
		}
			
		StringTokenizer st = new StringTokenizer(words," ");
		while(st.hasMoreTokens()){
			list.add(st.nextToken());
		}

		return list;
	}
	
	public ArrayList<String> generateNGgram(String words,int num)
	{	ArrayList<String> myword = stringTokenizer(words);
		int first = 0;
		ArrayList<String> list = new ArrayList<String>();
		String mywords ="";
	    for(String str: myword)
	        mywords=mywords+str;
		while (first != mywords.length()-2){
			String word = mywords.substring(first, num);
			first += 1;
			num += 1;
			list.add(word);
		}
		
		return list;
	}

}
